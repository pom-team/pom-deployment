import os

DEBUG = True 
SECRET_KEY = os.environ['SECRET_KEY']
NEVERCACHE_KEY = os.environ['NEVERCACHE_KEY']

ALLOWED_HOSTS = [os.environ['POM_SERVER_URL'], "127.0.0.1", "localhost",]

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'pom-db',
        'PORT': 5432,
    }
}