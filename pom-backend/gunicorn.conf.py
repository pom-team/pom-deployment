from __future__ import unicode_literals
import multiprocessing

bind = "unix:./gunicorn.sock"
workers = 4
errorlog = "pom_error.log"
loglevel = "error"
proc_name = "pom"
