python manage.py migrate
python manage.py collectstatic  --noinput

gunicorn -b 0.0.0.0:8000 --daemon --log-file=/var/log/gunicorn.log pom.wsgi:application 

nginx -g "daemon off;"