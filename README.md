# Palestine Open Maps deployment 
This is a deployment script for [Palestine Open Maps](palopenmaps.org) platform. It includes the following components: 

* POM front-end platform (vue.js)
* POM back-end platform (Django)
* Raster tile server ([tessera](https://github.com/mojodna/tessera/))
* Digitization infrastructure ([osm-seed](https://github.com/developmentseed/osm-seed/))